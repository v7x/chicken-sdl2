
(test-group "sdl2:get-hint"
  (sdl2:clear-hints!)

  (test "Returns #f if the hint is not set (symbol name)"
        #f
        (sdl2:get-hint 'render-scale-quality))

  (sdl2:set-hint! 'render-scale-quality "best")
  (test "returns the hint's value if the hint is set (symbol name)"
        "best"
        (sdl2:get-hint 'render-scale-quality))

  (test "returns #f if the hint is not set (string name)"
        #f
        (sdl2:get-hint "foo"))

  (sdl2:set-hint! "foo" "bar")
  (test "returns the hint's value if the hint is set (string name)"
        "bar"
        (sdl2:get-hint "foo")))



(test-group "sdl2:set-hint!"
  (sdl2:clear-hints!)

  (test "Returns #t if the hint was set"
        (list #t
              "0")
        (list (sdl2:set-hint! 'render-scale-quality "0" 'default)
              (sdl2:get-hint  'render-scale-quality)))

  (sdl2:set-hint! 'render-scale-quality "0" 'normal)
  (test "Returns #f if the hint was not set"
        #f
        (sdl2:set-hint! 'render-scale-quality "1" 'default))

  (sdl2:clear-hints!)
  (sdl2:set-hint! 'render-scale-quality "0" 'default)

  (test "Sets hint if given priority equal to current"
        (list #t
              "1")
        (list (sdl2:set-hint! 'render-scale-quality "1" 'default)
              (sdl2:get-hint  'render-scale-quality)))

  (test "Sets hint if given priority higher than current"
        (list #t
              "2"
              #t
              "3")
        (list (sdl2:set-hint! 'render-scale-quality "2" 'normal)
              (sdl2:get-hint  'render-scale-quality)
              (sdl2:set-hint! 'render-scale-quality "3" 'override)
              (sdl2:get-hint  'render-scale-quality)))

  (test "Does not set hint if given priority lower than current"
        (list #f
              #f
              "3")
        (list (sdl2:set-hint! 'render-scale-quality "4" 'default)
              (sdl2:set-hint! 'render-scale-quality "4" 'normal)
              (sdl2:get-hint  'render-scale-quality)))

  (sdl2:clear-hints!)

  (test "Priority defaults to normal"
        (list #t
              #f
              #t
              #t)
        (list (sdl2:set-hint! 'render-scale-quality "5")
              (sdl2:set-hint! 'render-scale-quality "5" 'default)
              (sdl2:set-hint! 'render-scale-quality "5" 'normal)
              (sdl2:set-hint! 'render-scale-quality "5" 'override)))

  (test-error "Signals error if value is not a string"
    (sdl2:set-hint! 'render-scale-quality 'best))

  (test-error "Signals error if hint name is unrecognized symbol"
    (sdl2:set-hint! 'foo "0"))

  (test "Accepts arbitrary string as hint name"
        (list #t
              "bar")
        (list (sdl2:set-hint! "foo" "bar")
              (sdl2:get-hint  "foo"))))



(test-group "sdl2:clear-hints!"
  (sdl2:set-hint! 'render-scale-quality "best" 'override)
  (sdl2:set-hint! "foo" "buz" 'override)

  (sdl2:clear-hints!)

  (test "Clears the value of all hints"
        (list #f
              #f)
        (list (sdl2:get-hint 'render-scale-quality)
              (sdl2:get-hint "foo")))

  (test-assert "Clears the priority of all hints"
    ;; This was previously set with priority 'override, so setting
    ;; with 'default could only succeed if old priority was cleared.
    (sdl2:set-hint! 'render-scale-quality "best" 'default)))

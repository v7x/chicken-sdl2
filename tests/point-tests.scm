
(test-group "sdl2:make-point"
  (test-assert (sdl2:point? (sdl2:make-point)))
  (test 0 (sdl2:point-x (sdl2:make-point)))
  (test 0 (sdl2:point-y (sdl2:make-point)))

  (test-assert (sdl2:point? (sdl2:make-point 1)))
  (test 1 (sdl2:point-x (sdl2:make-point 1)))
  (test 0 (sdl2:point-y (sdl2:make-point 1)))

  (test-assert (sdl2:point? (sdl2:make-point 1 2)))
  (test 1 (sdl2:point-x (sdl2:make-point 1 2)))
  (test 2 (sdl2:point-y (sdl2:make-point 1 2)))

  ;; Inexact integers
  (test-assert (sdl2:point? (sdl2:make-point 1.0 2.0)))
  (test 1 (sdl2:point-x (sdl2:make-point 1.0 2.0)))
  (test 2 (sdl2:point-y (sdl2:make-point 1.0 2.0))))


(test-group "sdl2:point?"
  (test-assert (sdl2:point? (sdl2:make-point)))
  (test-assert (sdl2:point? (sdl2:make-point 1 2)))
  (test-assert (not (sdl2:point? '(1 2))))
  (test-assert (not (sdl2:point? #(1 2))))
  (test-assert (not (sdl2:point? (sdl2:make-rect)))))


(test-integer-struct-fields
 make: (sdl2:make-point)
 freer: sdl2:free-point!
 (x
  getter: sdl2:point-x
  setter: sdl2:point-x-set!
  min: Sint32-min
  max: Sint32-max)
 (y
  getter: sdl2:point-y
  setter: sdl2:point-y-set!
  min: Sint32-min
  max: Sint32-max))


(test-group "sdl2:point-set!"
  (let ((point (sdl2:make-point)))
    (test-assert "returns the point"
      (eq? point (sdl2:point-set! point 3 4))))
  (test "sets all fields if all values are specified"
        '(3 4)
        (sdl2:point->list
         (sdl2:point-set! (sdl2:make-point 1 2) 3 4)))
  (test "accepts inexact integers"
        '(3 4)
        (sdl2:point->list
         (sdl2:point-set! (sdl2:make-point 1 2)
                          3.0 4.0)))
  (test "does not change fields where the value is omitted"
        '(3 2)
        (sdl2:point->list
         (sdl2:point-set! (sdl2:make-point 1 2) 3)))
  (test "has no effect if all values are omitted"
        '(1 2)
        (sdl2:point->list
         (sdl2:point-set! (sdl2:make-point 1 2))))
  (test "does not change fields where the value is #f"
        '(1 4)
        (sdl2:point->list
         (sdl2:point-set! (sdl2:make-point 1 2) #f 4)))
  (test "has no effect if all values are #f"
        '(1 2)
        (sdl2:point->list
         (sdl2:point-set! (sdl2:make-point 1 2) #f #f))))


(test-group "sdl2:free-point!"
  (let ((point (sdl2:make-point)))
    (sdl2:free-point! point)
    (test-assert "sets the record's pointer to null"
                 (sdl2:struct-null? point)))

  (let ((point (sdl2:make-point)))
    (test-assert "returns the same instance"
                 (eq? point (sdl2:free-point! point)))
    (test-assert "is safe to use multiple times on the same point"
                 (eq? point (sdl2:free-point! point))))

  (test-error (sdl2:free-point! 0))
  (test-error (sdl2:free-point! #f))
  (test-error (sdl2:free-point! '(1 2)))
  (test-error (sdl2:free-point! (sdl2:make-rect))))


(test-group "sdl2:point-copy"
  (test "Returns a different sdl2:point with the same values"
        (list #t '(1 2))
        (let* ((a (sdl2:make-point 1 2))
               (result (sdl2:point-copy a)))
          (list (not (sdl2:struct-eq? a result))
                (sdl2:point->list result)))))

(test-group "sdl2:point-copy!"
  (test "Modifies and returns the destination point"
        (list #t '(1 2))
        (let* ((a (sdl2:make-point 1 2))
               (b (sdl2:make-point 3 4))
               (result (sdl2:point-copy! a b)))
          (list (eq? b result)
                (sdl2:point->list b)))))



(test-group "sdl2:point-scale!"
  (test "Can scale the point by an exact integer"
        '(-18 36)
        (sdl2:point->list
         (sdl2:point-scale! (sdl2:make-point -3 6) 6)))

  (test "Can scale the point by an inexact integer"
        '(-18 36)
        (sdl2:point->list
         (sdl2:point-scale! (sdl2:make-point -3 6) 6.0)))

  (test "Can scale the point by a float (truncates)"
        '(-19 39)
        (sdl2:point->list
         (sdl2:point-scale! (sdl2:make-point -3 6) 6.6)))

  (test "Can scale the point by a large integer"
        '(-37037034 74074068)
        (sdl2:point->list
         (sdl2:point-scale! (sdl2:make-point -3 6) 12345678)))

  (test "Can scale the point by a large negative integer"
        '(37037034 -74074068)
        (sdl2:point->list
         (sdl2:point-scale! (sdl2:make-point -3 6) -12345678)))

  (test "Can scale the point by a large float (trucates)"
        '(-37037036 74074073)
        (sdl2:point->list
         (sdl2:point-scale! (sdl2:make-point -3 6) 12345678.9)))

  (test "Can scale the point by a large negative float (trucates)"
        '(37037036 -74074073)
        (sdl2:point->list
         (sdl2:point-scale! (sdl2:make-point -3 6) -12345678.9)))

  (test-assert "Modifies and returns the same point by default"
    (let ((p (sdl2:make-point -3 6)))
      (eq? p (sdl2:point-scale! p -6.6))))

  (test "Modifies and returns the given dest point"
        (list #t '(-19 39))
        (let ((p (sdl2:make-point -3 6))
              (dest (sdl2:make-point 123 456)))
          (list (eq? dest (sdl2:point-scale! p 6.6 dest))
                (sdl2:point->list dest)))))


(test-group "sdl2:point-scale"
  (test "Returns a new point without modifying the original"
        '(( -3  6)
          (-19 39))
        (let* ((p (sdl2:make-point -3 6))
               (result (sdl2:point-scale p 6.6)))
          (list (sdl2:point->list p)
                (sdl2:point->list result)))))



(test-group "sdl2:point-unscale!"
  (test "Can scale the point by an exact integer"
        '(-3 6)
        (sdl2:point->list
         (sdl2:point-unscale! (sdl2:make-point -18 36) 6)))

  (test "Can scale the point by an inexact integer"
        '(-3 6)
        (sdl2:point->list
         (sdl2:point-unscale! (sdl2:make-point -18 36) 6.0)))

  (test "Can scale the point by a float (truncates)"
        '(-2 5)
        (sdl2:point->list
         (sdl2:point-unscale! (sdl2:make-point -19 39) 6.6)))

  (test "Can scale the point by a large integer"
        '(-3 6)
        (sdl2:point->list
         (sdl2:point-unscale! (sdl2:make-point -37037034 74074068)
                              12345678)))

  (test "Can scale the point by a large negative integer"
        '(-3 6)
        (sdl2:point->list
         (sdl2:point-unscale! (sdl2:make-point 37037034 -74074068)
                              -12345678)))

  (test "Can scale the point by a large float (trucates)"
        '(-2 5)
        (sdl2:point->list
         (sdl2:point-unscale! (sdl2:make-point -37037036 74074073)
                              12345678.9)))

  (test "Can scale the point by a large negative float (trucates)"
        '(-2 5)
        (sdl2:point->list
         (sdl2:point-unscale! (sdl2:make-point 37037036 -74074073)
                              -12345678.9)))

  (test-assert "Modifies and returns the same point by default"
    (let ((p (sdl2:make-point -3 6)))
      (eq? p (sdl2:point-unscale! p -6.6))))

  (test "Modifies and returns the given dest point"
        (list #t '(-2 5))
        (let ((p (sdl2:make-point -19 39))
              (dest (sdl2:make-point 123 456)))
          (list (eq? dest (sdl2:point-unscale! p 6.6 dest))
                (sdl2:point->list dest))))

  (test-error "Errors if scale is 0"
    (sdl2:point-unscale! (sdl2:make-point 1 2) 0))
  (test-error "Errors if scale is 0.0"
    (sdl2:point-unscale! (sdl2:make-point 1 2) 0.0)))


(test-group "sdl2:point-unscale"
  (test "Returns a new point without modifying the original"
        '((-19 39)
          ( -2  5))
        (let* ((p (sdl2:make-point -19 39))
               (result (sdl2:point-unscale p 6.6)))
          (list (sdl2:point->list p)
                (sdl2:point->list result))))

  (test-error "Errors if scale is 0"
    (sdl2:point-unscale (sdl2:make-point 1 2) 0))
  (test-error "Errors if scale is 0.0"
    (sdl2:point-unscale (sdl2:make-point 1 2) 0.0)))



(test-group "sdl2:point-move!"
  (test "Can move the point by exact integers"
        '(3 -2)
        (sdl2:point->list
         (sdl2:point-move! (sdl2:make-point -3 6) 6 -8)))

  (test "Can move the point by inexact integers"
        '(3 -2)
        (sdl2:point->list
         (sdl2:point-move! (sdl2:make-point -3 6) 6.0 -8.0)))

  (test-error "Signals error if given a float"
    (sdl2:point-move! (sdl2:make-point -3 6) 6.6 -8))

  (test-assert "Modifies and returns the same point by default"
    (let ((p (sdl2:make-point -3 6)))
      (eq? p (sdl2:point-move! p 6 -8))))

  (test "Modifies and returns the given dest point"
        (list #t '(3 -2))
        (let ((p (sdl2:make-point -3 6))
              (dest (sdl2:make-point 123 456)))
          (list (eq? dest (sdl2:point-move! p 6 -8 dest))
                (sdl2:point->list dest)))))


(test-group "sdl2:point-move"
  (test "Returns a new point without modifying the original"
        '((-3  6)
          ( 3 -2))
        (let* ((p (sdl2:make-point -3 6))
               (result (sdl2:point-move p 6 -8)))
          (list (sdl2:point->list p)
                (sdl2:point->list result)))))



(test-group "sdl2:point-add!"
  (test "Adds the two points together and returns the result"
        '(3 -2)
        (sdl2:point->list
         (sdl2:point-add! (sdl2:make-point -3 6)
                          (sdl2:make-point 6 -8))))

  (test-assert "Modifies and returns the first point by default"
    (let ((p1 (sdl2:make-point -3 6))
          (p2 (sdl2:make-point 6 -8)))
      (eq? p1 (sdl2:point-add! p1 p2))))

  (test "Modifies and returns the given dest point"
        (list #t '(3 -2))
        (let ((p1 (sdl2:make-point -3 6))
              (p2 (sdl2:make-point 6 -8))
              (dest (sdl2:make-point 123 456)))
          (list (eq? dest (sdl2:point-add! p1 p2 dest))
                (sdl2:point->list dest)))))


(test-group "sdl2:point-add"
  (test "Returns a new point without modifying the originals"
        '((-3  6)
          ( 6 -8)
          ( 3 -2))
        (let* ((p1 (sdl2:make-point -3  6))
               (p2 (sdl2:make-point  6 -8))
               (result (sdl2:point-add p1 p2)))
          (list (sdl2:point->list p1)
                (sdl2:point->list p2)
                (sdl2:point->list result)))))


(test-group "sdl2:point-sub!"
  (test "Subtracts the second point from the first point"
        '(-9 14)
        (sdl2:point->list
         (sdl2:point-sub! (sdl2:make-point -3 6)
                          (sdl2:make-point 6 -8))))

  (test-assert "Modifies and returns the first point by default"
    (let ((p1 (sdl2:make-point -3 6))
          (p2 (sdl2:make-point 6 -8)))
      (eq? p1 (sdl2:point-sub! p1 p2))))

  (test "Modifies and returns the given dest point"
        (list #t '(-9 14))
        (let ((p1 (sdl2:make-point -3 6))
              (p2 (sdl2:make-point 6 -8))
              (dest (sdl2:make-point 123 456)))
          (list (eq? dest (sdl2:point-sub! p1 p2 dest))
                (sdl2:point->list dest)))))


(test-group "sdl2:point-sub"
  (test "Returns a new point without modifying the originals"
        '((-3  6)
          ( 6 -8)
          (-9 14))
        (let* ((p1 (sdl2:make-point -3  6))
               (p2 (sdl2:make-point  6 -8))
               (result (sdl2:point-sub p1 p2)))
          (list (sdl2:point->list p1)
                (sdl2:point->list p2)
                (sdl2:point->list result)))))



(test-group "sdl2:point-lerp!"
  (test "t between 0 and 1 interpolates between the points"
        '(-93 248)
        (sdl2:point->list
         (sdl2:point-lerp! (sdl2:make-point -325  615)
                           (sdl2:make-point  600 -850)
                           0.25)))

  (test "t = 0 results in same as first point"
        '(-325 615)
        (sdl2:point->list
         (sdl2:point-lerp! (sdl2:make-point -325  615)
                           (sdl2:make-point  600 -850)
                           0)))

  (test "t = 1 results in same as second point"
        '(600 -850)
        (sdl2:point->list
         (sdl2:point-lerp! (sdl2:make-point -325  615)
                           (sdl2:make-point  600 -850)
                           1)))

  (test "t < 0 extrapolates beyond the first point"
        '(-1250 2080)
        (sdl2:point->list
         (sdl2:point-lerp! (sdl2:make-point -325  615)
                           (sdl2:make-point  600 -850)
                           -1)))

  (test "t > 1 extrapolates beyond the second point"
        '(1525 -2315)
        (sdl2:point->list
         (sdl2:point-lerp! (sdl2:make-point -325  615)
                           (sdl2:make-point  600 -850)
                           2)))

  (test-assert "Modifies and returns the first point by default"
    (let ((p1 (sdl2:make-point -325  615))
          (p2 (sdl2:make-point  600 -850)))
      (eq? p1 (sdl2:point-lerp! p1 p2 0.25))))

  (test "Modifies and returns the given dest point"
        (list #t '(-93 248))
        (let ((p1 (sdl2:make-point -325  615))
              (p2 (sdl2:make-point  600 -850))
              (dest (sdl2:make-point 123 456)))
          (list (eq? dest (sdl2:point-lerp! p1 p2 0.25 dest))
                (sdl2:point->list dest)))))


(test-group "sdl2:point-lerp"
  (test "Returns a new point without modifying the originals"
        '((-325  615)
          ( 600 -850)
          ( -93  248))
        (let* ((p1 (sdl2:make-point -325  615))
               (p2 (sdl2:make-point  600 -850))
               (result (sdl2:point-lerp p1 p2 0.25)))
          (list (sdl2:point->list p1)
                (sdl2:point->list p2)
                (sdl2:point->list result)))))

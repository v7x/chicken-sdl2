
(test-group "sdl2:struct-eq?"
  (test-group "Record containing a blob"
    (test "Returns #t when given the same record twice"
          #t
          (let ((x (sdl2:make-rect)))
            (sdl2:struct-eq? x x)))

    (test "Returns #t for different records with same blob"
          #t
          (let ((x (sdl2:make-rect)))
            (sdl2:struct-eq? x (SDL:wrap-rect
                                (SDL:%rect-pointer x)))))

    (test "Returns #f for different records with same values"
          #f
          (sdl2:struct-eq? (sdl2:make-rect 1 2 3 4)
                           (sdl2:make-rect 1 2 3 4)))

    (test "Returns #t when comparing struct versus same locative"
          #t
          (let ((x (sdl2:make-rect)))
            (sdl2:struct-eq? x (SDL:%rect-pointer x))))

    (test "Returns #t when comparing struct versus same blob"
          #t
          (let ((x (sdl2:make-rect)))
            (sdl2:struct-eq? x (SDL:%rect-pointer x))))

    (test "Returns #t when comparing to pointer with same address"
          #t
          (let ((x (sdl2:make-rect)))
            (sdl2:struct-eq? x (address->pointer
                                (pointer->address
                                 (make-locative
                                  (SDL:%rect-pointer x)))))))

    (test "Returns #f when comparing to #f"
          #f
          (let ((x (sdl2:make-rect)))
            (sdl2:struct-eq? x #f)))

    (test-error "Error when comparing to other object type (string)"
      (let ((x (sdl2:make-rect)))
        (sdl2:struct-eq? x "foo")))

    (test-error "Error when comparing to other object type (number)"
      (let ((x (sdl2:make-rect)))
        (sdl2:struct-eq? x 0))))

  (test-group "Record containing a pointer"
    (test "Returns #t when given the same record twice"
          #t
          (let ((x (sdl2:make-surface 1 1 24)))
            (sdl2:struct-eq? x x)))

    (test "Returns #t for different records with same pointer"
          #t
          (let ((x (sdl2:make-surface 1 1 24)))
            (sdl2:struct-eq? x (SDL:wrap-rect
                                (SDL:%surface-pointer x)))))

    (test "Returns #f for different records with same values"
          #f
          (sdl2:struct-eq? (sdl2:make-surface 1 1 24)
                           (sdl2:make-surface 1 1 24)))

    (test "Returns #t when comparing to pointer with same address"
          #t
          (let ((x (sdl2:make-surface 1 1 24)))
            (sdl2:struct-eq? x (address->pointer
                                (pointer->address
                                 (SDL:%surface-pointer x))))))

    (test "Returns #f when comparing to #f"
          #f
          (let ((x (sdl2:make-surface 1 1 24)))
            (sdl2:struct-eq? x #f)))

    (test-error "Error when comparing to other object type (string)"
      (let ((x (sdl2:make-surface 1 1 24)))
        (sdl2:struct-eq? x "foo")))

    (test-error "Error when comparing to other object type (number)"
      (let ((x (sdl2:make-surface 1 1 24)))
        (sdl2:struct-eq? x 0))))

  (test-group "Record containing a null pointer"
    (test "Returns #t when compared to #f"
          #t
          (sdl2:struct-eq? (SDL:wrap-surface (address->pointer 0)) #f)))

  (test "Returns #t comparing null pointer with null pointer"
        #t
        (sdl2:struct-eq? (address->pointer 0) (address->pointer 0)))

  (test "Returns #t comparing null pointer with #f"
        #t
        (sdl2:struct-eq? (address->pointer 0) #f))

  (test "Returns #t comparing #f with #f"
        #t
        (sdl2:struct-eq? #f #f)))



(test-group "sdl2:struct-null?"
  (test-group "with sdl2:color"
    (test "returns #t if record pointer is null"
          #t
          (sdl2:struct-null?
           (SDL:wrap-color (address->pointer 0))))
    (test "returns #f if record pointer is non-null"
          #f (sdl2:struct-null? (sdl2:make-color))))

  (test-group "with sdl2:event"
    (test "returns #t if record pointer is null"
          #t
          (sdl2:struct-null?
           (SDL:wrap-event (address->pointer 0))))
    (test "returns #f if record pointer is non-null"
          #f
          (sdl2:struct-null? (sdl2:make-event))))

  (test-group "with sdl2:point"
    (test "returns #t if record pointer is null"
          #t
          (sdl2:struct-null?
           (SDL:wrap-point (address->pointer 0))))
    (test "returns #f if record pointer is non-null"
          #f
          (sdl2:struct-null? (sdl2:make-point))))

  (test-group "with sdl2:rect"
    (test "returns #t if record pointer is null"
          #t
          (sdl2:struct-null?
           (SDL:wrap-rect (address->pointer 0))))
    (test "returns #f if record pointer is non-null"
          #f
          (sdl2:struct-null? (sdl2:make-rect))))

  (test-group "with sdl2:surface"
    (test "returns #t if record pointer is null"
          #t
          (sdl2:struct-null?
           (SDL:wrap-surface (address->pointer 0))))
    (let ((surface (sdl2:make-surface 1 1 16)))
      (test "returns #f if record pointer is non-null"
            #f
            (sdl2:struct-null? surface))))

  (test-group "with invalid types"
    (test-error "throws error if given a non-null pointer"
      (sdl2:struct-null?
       (sdl2:unwrap-rect (sdl2:make-rect))))
    (test-error "throws error if given a null pointer"
      (sdl2:struct-null? (address->pointer 0)))
    (test-error "throws error if given #f"
      (sdl2:struct-null? #f))
    (test-error "throws error if given a number"
      (sdl2:struct-null? 0))))

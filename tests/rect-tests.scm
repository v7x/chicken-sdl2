
(test-group "sdl2:make-rect"
  (test-assert (sdl2:rect? (sdl2:make-rect)))
  (test 0 (sdl2:rect-x (sdl2:make-rect)))
  (test 0 (sdl2:rect-y (sdl2:make-rect)))
  (test 0 (sdl2:rect-w (sdl2:make-rect)))
  (test 0 (sdl2:rect-h (sdl2:make-rect)))

  (test-assert (sdl2:rect? (sdl2:make-rect 1)))
  (test 1 (sdl2:rect-x (sdl2:make-rect 1)))
  (test 0 (sdl2:rect-y (sdl2:make-rect 1)))
  (test 0 (sdl2:rect-w (sdl2:make-rect 1)))
  (test 0 (sdl2:rect-h (sdl2:make-rect 1)))

  (test-assert (sdl2:rect? (sdl2:make-rect 1 2 3 4)))
  (test 1 (sdl2:rect-x (sdl2:make-rect 1 2 3 4)))
  (test 2 (sdl2:rect-y (sdl2:make-rect 1 2 3 4)))
  (test 3 (sdl2:rect-w (sdl2:make-rect 1 2 3 4)))
  (test 4 (sdl2:rect-h (sdl2:make-rect 1 2 3 4)))

  ;; Inexact integers
  (test-assert (sdl2:rect? (sdl2:make-rect 1.0 2.0 3.0 4.0)))
  (test 1 (sdl2:rect-x (sdl2:make-rect 1.0 2.0 3.0 4.0)))
  (test 2 (sdl2:rect-y (sdl2:make-rect 1.0 2.0 3.0 4.0)))
  (test 3 (sdl2:rect-w (sdl2:make-rect 1.0 2.0 3.0 4.0)))
  (test 4 (sdl2:rect-h (sdl2:make-rect 1.0 2.0 3.0 4.0))))


(test-group "sdl2:rect?"
  (test-assert (sdl2:rect? (sdl2:make-rect)))
  (test-assert (sdl2:rect? (sdl2:make-rect 1 2 3 4)))
  (test-assert (not (sdl2:rect? '(1 2 3 4))))
  (test-assert (not (sdl2:rect? #(1 2 3 4))))
  (test-assert (not (sdl2:rect? (sdl2:make-point)))))


(test-integer-struct-fields
 make: (sdl2:make-rect)
 freer: sdl2:free-rect!
 (x
  getter: sdl2:rect-x
  setter: sdl2:rect-x-set!
  min: Sint32-min
  max: Sint32-max)
 (y
  getter: sdl2:rect-y
  setter: sdl2:rect-y-set!
  min: Sint32-min
  max: Sint32-max)
 (w
  getter: sdl2:rect-w
  setter: sdl2:rect-w-set!
  min: Sint32-min
  max: Sint32-max)
 (h
  getter: sdl2:rect-h
  setter: sdl2:rect-h-set!
  min: Sint32-min
  max: Sint32-max))


(test-group "sdl2:rect-set!"
  (let ((rect (sdl2:make-rect)))
    (test-assert "returns the rect"
                 (eq? rect (sdl2:rect-set! rect 5 6 7 8))))
  (test "sets all fields if all values are specified"
        '(5 6 7 8)
        (sdl2:rect->list
         (sdl2:rect-set! (sdl2:make-rect 1 2 3 4) 5 6 7 8)))
  (test "accepts inexact integers"
        '(5 6 7 8)
        (sdl2:rect->list
         (sdl2:rect-set! (sdl2:make-rect 1 2 3 4)
                         5.0 6.0 7.0 8.0)))
  (test "does not change fields where the value is omitted"
        '(5 6 3 4)
        (sdl2:rect->list
         (sdl2:rect-set! (sdl2:make-rect 1 2 3 4) 5 6)))
  (test "has no effect if all values are omitted"
        '(1 2 3 4)
        (sdl2:rect->list
         (sdl2:rect-set! (sdl2:make-rect 1 2 3 4))))
  (test "does not change fields where the value is #f"
        '(1 8 3 9)
        (sdl2:rect->list
         (sdl2:rect-set! (sdl2:make-rect 1 2 3 4) #f 8 #f 9)))
  (test "has no effect if all values are #f"
        '(1 2 3 4)
        (sdl2:rect->list
         (sdl2:rect-set! (sdl2:make-rect 1 2 3 4) #f #f #f #f))))


(test-group "sdl2:free-rect!"
  (let ((rect (sdl2:make-rect)))
    (sdl2:free-rect! rect)
    (test-assert "sets the record's pointer to null"
                 (sdl2:struct-null? rect)))

  (let ((rect (sdl2:make-rect)))
    (test-assert "returns the same instance"
                 (eq? rect (sdl2:free-rect! rect)))
    (test-assert "is safe to use multiple times on the same rect"
                 (eq? rect (sdl2:free-rect! rect))))

  (test-error (sdl2:free-rect! 0))
  (test-error (sdl2:free-rect! #f))
  (test-error (sdl2:free-rect! '(1 2 3 4)))
  (test-error (sdl2:free-rect! (sdl2:make-point))))


(test-group "sdl2:rect-copy"
  (test "Returns a different sdl2:rect with the same values"
        (list #t '(1 2 3 4))
        (let* ((a (sdl2:make-rect 1 2 3 4))
               (result (sdl2:rect-copy a)))
          (list (not (sdl2:struct-eq? a result))
                (sdl2:rect->list result)))))

(test-group "sdl2:rect-copy!"
  (test "Modifies and returns the destination rect"
        (list #t '(1 2 3 4))
        (let* ((a (sdl2:make-rect 1 2 3 4))
               (b (sdl2:make-rect 5 6 7 8))
               (result (sdl2:rect-copy! a b)))
          (list (eq? b result)
                (sdl2:rect->list b)))))



(test-group "sdl2:rect-scale!"
  (test "Can scale the rect by an exact integer"
        '(-18 36 30 54)
        (sdl2:rect->list
         (sdl2:rect-scale! (sdl2:make-rect -3 6 5 9) 6)))

  (test "Can scale the rect by an inexact integer"
        '(-18 36 30 54)
        (sdl2:rect->list
         (sdl2:rect-scale! (sdl2:make-rect -3 6 5 9) 6.0)))

  (test "Can scale the rect by a float (truncates)"
        '(-19 39 33 59)
        (sdl2:rect->list
         (sdl2:rect-scale! (sdl2:make-rect -3 6 5 9) 6.6)))

  (test "Can scale the rect by a large integer"
        '(-37037034 74074068 61728390 111111102)
        (sdl2:rect->list
         (sdl2:rect-scale! (sdl2:make-rect -3 6 5 9) 12345678)))

  (test "Can scale the rect by a large negative integer"
        '(37037034 -74074068 -61728390 -111111102)
        (sdl2:rect->list
         (sdl2:rect-scale! (sdl2:make-rect -3 6 5 9) -12345678)))

  (test "Can scale the rect by a large float (truncates)"
        '(-37037036 74074073 61728394 111111110)
        (sdl2:rect->list
         (sdl2:rect-scale! (sdl2:make-rect -3 6 5 9) 12345678.9)))

  (test "Can scale the rect by a large negative float (truncates)"
        '(37037036 -74074073 -61728394 -111111110)
        (sdl2:rect->list
         (sdl2:rect-scale! (sdl2:make-rect -3 6 5 9) -12345678.9)))


  (test-assert "Modifies and returns the same rect by default"
    (let ((r (sdl2:make-rect -3 6 5 9)))
      (eq? r (sdl2:rect-scale! r -6.6))))

  (test "Modifies and returns the given dest rect"
        (list #t '(-19 39 33 59))
        (let ((r (sdl2:make-rect -3 6 5 9))
              (dest (sdl2:make-rect 12 34 56 78)))
          (list (eq? dest (sdl2:rect-scale! r 6.6 dest))
                (sdl2:rect->list dest)))))


(test-group "sdl2:rect-scale"
  (test "Returns a new rect without modifying the original"
        '(( -3  6  5  9)
          (-19 39 33 59))
        (let* ((r (sdl2:make-rect -3 6 5 9))
               (result (sdl2:rect-scale r 6.6)))
          (list (sdl2:rect->list r)
                (sdl2:rect->list result)))))



(test-group "sdl2:rect-unscale!"
  (test "Can scale the rect by an exact integer"
        '(-3 6 5 9)
        (sdl2:rect->list
         (sdl2:rect-unscale! (sdl2:make-rect -18 36 30 54) 6)))

  (test "Can scale the rect by an inexact integer"
        '(-3 6 5 9)
        (sdl2:rect->list
         (sdl2:rect-unscale! (sdl2:make-rect -18 36 30 54) 6.0)))

  (test "Can scale the rect by a float (truncates)"
        '(-2 5 5 8)
        (sdl2:rect->list
         (sdl2:rect-unscale! (sdl2:make-rect -19 39 33 59) 6.6)))

  (test "Can scale the rect by a large integer"
        '(-3 6 5 9)
        (sdl2:rect->list
         (sdl2:rect-unscale!
          (sdl2:make-rect -37037034 74074068 61728390 111111102)
          12345678)))

  (test "Can scale the rect by a large negative integer"
        '(-3 6 5 9)
        (sdl2:rect->list
         (sdl2:rect-unscale!
          (sdl2:make-rect 37037034 -74074068 -61728390 -111111102)
          -12345678)))

  (test "Can scale the rect by a large float (truncates)"
        '(-2 5 4 8)
        (sdl2:rect->list
         (sdl2:rect-unscale!
          (sdl2:make-rect -37037036 74074073 61728394 111111110)
          12345678.9)))

  (test "Can scale the rect by a large negative float (truncates)"
        '(-2 5 4 8)
        (sdl2:rect->list
         (sdl2:rect-unscale!
          (sdl2:make-rect 37037036 -74074073 -61728394 -111111110)
          -12345678.9)))


  (test-assert "Modifies and returns the same rect by default"
    (let ((r (sdl2:make-rect -3 6 5 9)))
      (eq? r (sdl2:rect-unscale! r -6.6))))

  (test "Modifies and returns the given dest rect"
        (list #t '(-2 5 5 8))
        (let ((r (sdl2:make-rect -19 39 33 59))
              (dest (sdl2:make-rect 12 34 56 78)))
          (list (eq? dest (sdl2:rect-unscale! r 6.6 dest))
                (sdl2:rect->list dest))))

  (test-error "Errors if scale is 0"
    (sdl2:rect-unscale! (sdl2:make-rect 1 2 3 4) 0))
  (test-error "Errors if scale is 0.0"
    (sdl2:rect-unscale! (sdl2:make-rect 1 2 3 4) 0.0)))


(test-group "sdl2:rect-unscale"
  (test "Returns a new rect without modifying the original"
        '((-19 39 33 59)
          ( -2  5  5  8))
        (let* ((r (sdl2:make-rect -19 39 33 59))
               (result (sdl2:rect-unscale r 6.6)))
          (list (sdl2:rect->list r)
                (sdl2:rect->list result))))

  (test-error "Errors if scale is 0"
    (sdl2:rect-unscale (sdl2:make-rect 1 2 3 4) 0))
  (test-error "Errors if scale is 0.0"
    (sdl2:rect-unscale (sdl2:make-rect 1 2 3 4) 0.0)))



(test-group "sdl2:rect-move!"
  (test "Can move the rect by exact integers"
        '(3 -2 12 34)
        (sdl2:rect->list
         (sdl2:rect-move! (sdl2:make-rect -3 6 12 34)
                          6 -8)))

  (test "Can move the rect by inexact integers"
        '(3 -2 12 34)
        (sdl2:rect->list
         (sdl2:rect-move! (sdl2:make-rect -3 6 12 34)
                          6.0 -8.0)))

  (test-error "Signals error if given a float"
    (sdl2:rect-move! (sdl2:make-rect -3 6 12 34)
                     6.6 -8))

  (test-assert "Modifies and returns the same rect by default"
    (let ((r (sdl2:make-rect -3 6 12 34)))
      (eq? r (sdl2:rect-move! r 6 -8))))

  (test "Modifies and returns the given dest rect"
        (list #t '(3 -2 12 34))
        (let ((r (sdl2:make-rect -3 6 12 34))
              (dest (sdl2:make-rect 12 34 56 78)))
          (list (eq? dest (sdl2:rect-move! r 6 -8 dest))
                (sdl2:rect->list dest)))))


(test-group "sdl2:rect-move"
  (test "Returns a new rect without modifying the original"
        '((-3  6 12 34)
          ( 3 -2 12 34))
        (let* ((r (sdl2:make-rect -3 6 12 34))
               (result (sdl2:rect-move r 6 -8)))
          (list (sdl2:rect->list r)
                (sdl2:rect->list result)))))



(test-group "sdl2:rect-add-point!"
  (test "Adds the two rects together and returns the result"
        '(3 -2 12 34)
        (sdl2:rect->list
         (sdl2:rect-add-point!
          (sdl2:make-rect -3 6 12 34)
          (sdl2:make-point 6 -8))))

  (test-assert "Modifies and returns the rect by default"
    (let ((r (sdl2:make-rect -3 6 12 34))
          (p (sdl2:make-point 6 -8)))
      (eq? r (sdl2:rect-add-point! r p))))

  (test "Modifies and returns the given dest rect"
        (list #t '(3 -2 12 34))
        (let ((r (sdl2:make-rect -3 6 12 34))
              (p (sdl2:make-point 6 -8))
              (dest (sdl2:make-rect 12 34 56 78)))
          (list (eq? dest (sdl2:rect-add-point! r p dest))
                (sdl2:rect->list dest)))))


(test-group "sdl2:rect-add-point"
  (test "Returns a new rect without modifying the originals"
        '((-3  6 12 34)
          ( 6 -8)
          ( 3 -2 12 34))
        (let* ((r (sdl2:make-rect -3 6 12 34))
               (p (sdl2:make-point 6 -8))
               (result (sdl2:rect-add-point r p)))
          (list (sdl2:rect->list r)
                (sdl2:point->list p)
                (sdl2:rect->list result)))))



(test-group "sdl2:rect-sub-point!"
  (test "Subtracts the second rect from the first rect"
        '(-9 14 12 34)
        (sdl2:rect->list
         (sdl2:rect-sub-point!
          (sdl2:make-rect -3 6 12 34)
          (sdl2:make-point 6 -8))))

  (test-assert "Modifies and returns the rect by default"
    (let ((r (sdl2:make-rect -3 6 12 34))
          (p (sdl2:make-point 6 -8)))
      (eq? r (sdl2:rect-sub-point! r p))))

  (test "Modifies and returns the given dest rect"
        (list #t '(-9 14 12 34))
        (let ((r (sdl2:make-rect -3 6 12 34))
              (p (sdl2:make-point 6 -8))
              (dest (sdl2:make-rect 12 34 56 78)))
          (list (eq? dest (sdl2:rect-sub-point! r p dest))
                (sdl2:rect->list dest)))))


(test-group "sdl2:rect-sub-point"
  (test "Returns a new rect without modifying the originals"
        '((-3  6 12 34)
          ( 6 -8)
          (-9 14 12 34))
        (let* ((r (sdl2:make-rect -3 6 12 34))
               (p (sdl2:make-point 6 -8))
               (result (sdl2:rect-sub-point r p)))
          (list (sdl2:rect->list r)
                (sdl2:point->list p)
                (sdl2:rect->list result)))))



(test-group "sdl2:rect-grow!"
  (test "Can grow the rect by exact integers"
        '(12 34 3 -2)
        (sdl2:rect->list
         (sdl2:rect-grow! (sdl2:make-rect 12 34 -3 6)
                          6 -8)))

  (test "Can grow the rect by inexact integers"
        '(12 34 3 -2)
        (sdl2:rect->list
         (sdl2:rect-grow! (sdl2:make-rect 12 34 -3 6)
                          6.0 -8.0)))

  (test-error "Signals error if given a float"
    (sdl2:rect-grow! (sdl2:make-rect 12 34 -3 6)
                     6.6 -8))

  (test-assert "Modifies and returns the same rect by default"
    (let ((r (sdl2:make-rect 12 34 -3 6)))
      (eq? r (sdl2:rect-grow! r 6 -8))))

  (test "Modifies and returns the given dest rect"
        (list #t '(12 34 3 -2))
        (let ((r (sdl2:make-rect 12 34 -3 6))
              (dest (sdl2:make-rect 12 34 56 78)))
          (list (eq? dest (sdl2:rect-grow! r 6 -8 dest))
                (sdl2:rect->list dest)))))


(test-group "sdl2:rect-grow"
  (test "Returns a new rect without modifying the originals"
        '((12 34 -3 6)
          (12 34 3 -2))
        (let* ((r (sdl2:make-rect 12 34 -3 6))
               (result (sdl2:rect-grow r 6 -8)))
          (list (sdl2:rect->list r)
                (sdl2:rect->list result)))))



(test-group "sdl2:rect-grow/center!"
  (test "Can grow the rect by exact integers"
        '(-6 10 18 26)
        (sdl2:rect->list
         (sdl2:rect-grow/center!
          (sdl2:make-rect -3 6 12 34)
          6 -8)))

  (test "Odd integers change x/y by a truncated amount"
        '(-6 10 19 25)
        (sdl2:rect->list
         (sdl2:rect-grow/center!
          (sdl2:make-rect -3 6 12 34)
          7 -9)))

  (test "Can grow the rect by inexact integers"
        '(-6 10 18 26)
        (sdl2:rect->list
         (sdl2:rect-grow/center!
          (sdl2:make-rect -3 6 12 34)
          6.0 -8.0)))

  (test-error "Signals error if given a float"
    (sdl2:rect-grow/center!
     (sdl2:make-rect -3 6 12 34)
     6.6 -8))

  (test-assert "Modifies and returns the same rect by default"
    (let ((r (sdl2:make-rect -3 6 12 34)))
      (eq? r (sdl2:rect-grow/center! r 6 -8))))

  (test "Modifies and returns the given dest rect"
        (list #t '(-6 10 18 26))
        (let ((r (sdl2:make-rect -3 6 12 34))
              (dest (sdl2:make-rect 12 34 56 78)))
          (list (eq? dest (sdl2:rect-grow/center! r 6 -8 dest))
                (sdl2:rect->list dest)))))


(test-group "sdl2:rect-grow/center"
  (test "Returns a new rect without modifying the originals"
        '((-3 6 12 34)
          (-6 10 18 26))
        (let* ((r (sdl2:make-rect -3 6 12 34))
               (result (sdl2:rect-grow/center r 6 -8)))
          (list (sdl2:rect->list r)
                (sdl2:rect->list result)))))

(test-group "sdl2:rect-lerp!"
  (test "t between 0 and 1 interpolates between the rects"
        '(-93 248 23 45)
        (sdl2:rect->list
         (sdl2:rect-lerp! (sdl2:make-rect -325  615 12 34)
                          (sdl2:make-rect  600 -850 56 78)
                          0.25)))

  (test "t = 0 results in same as first rect"
        '(-325 615 12 34)
        (sdl2:rect->list
         (sdl2:rect-lerp! (sdl2:make-rect -325  615 12 34)
                          (sdl2:make-rect  600 -850 56 78)
                          0)))

  (test "t = 1 results in same as second rect"
        '(600 -850 56 78)
        (sdl2:rect->list
         (sdl2:rect-lerp! (sdl2:make-rect -325  615 12 34)
                          (sdl2:make-rect  600 -850 56 78)
                          1)))

  (test "t < 0 extrapolates beyond the first rect"
        '(-1250 2080 -32 -10)
        (sdl2:rect->list
         (sdl2:rect-lerp! (sdl2:make-rect -325  615 12 34)
                          (sdl2:make-rect  600 -850 56 78)
                          -1)))

  (test "t > 1 extrapolates beyond the second rect"
        '(1525 -2315 100 122)
        (sdl2:rect->list
         (sdl2:rect-lerp! (sdl2:make-rect -325  615 12 34)
                          (sdl2:make-rect  600 -850 56 78)
                          2)))

  (test-assert "Modifies and returns the first rect by default"
    (let ((r1 (sdl2:make-rect -325  615 12 34))
          (r2 (sdl2:make-rect  600 -850 56 78)))
      (eq? r1 (sdl2:rect-lerp! r1 r2 0.25))))

  (test "Modifies and returns the given dest rect"
        (list #t '(-93 248 23 45))
        (let ((r1 (sdl2:make-rect -325  615 12 34))
              (r2 (sdl2:make-rect  600 -850 56 78))
              (dest (sdl2:make-rect 123 456)))
          (list (eq? dest (sdl2:rect-lerp! r1 r2 0.25 dest))
                (sdl2:rect->list dest)))))


(test-group "sdl2:rect-lerp"
  (test "Returns a new rect without modifying the originals"
        '((-325  615 12 34)
          ( 600 -850 56 78)
          ( -93  248 23 45))
        (let* ((r1 (sdl2:make-rect -325  615 12 34))
               (r2 (sdl2:make-rect  600 -850 56 78))
               (result (sdl2:rect-lerp r1 r2 0.25)))
          (list (sdl2:rect->list r1)
                (sdl2:rect->list r2)
                (sdl2:rect->list result)))))



(test-group "sdl2:rect-lerp-xy!"
  (test "t between 0 and 1 interpolates between the rects's positions"
        '(-93 248 12 34)
        (sdl2:rect->list
         (sdl2:rect-lerp-xy! (sdl2:make-rect -325  615 12 34)
                             (sdl2:make-rect  600 -850 56 78)
                             0.25)))

  (test "t = 0 results in same position as first rect"
        '(-325 615 12 34)
        (sdl2:rect->list
         (sdl2:rect-lerp-xy! (sdl2:make-rect -325  615 12 34)
                             (sdl2:make-rect  600 -850 56 78)
                             0)))

  (test "t = 1 results in same position as second rect"
        '(600 -850 12 34)
        (sdl2:rect->list
         (sdl2:rect-lerp-xy! (sdl2:make-rect -325  615 12 34)
                             (sdl2:make-rect  600 -850 56 78)
                             1)))

  (test "t < 0 extrapolates beyond the first rect"
        '(-1250 2080 12 34)
        (sdl2:rect->list
         (sdl2:rect-lerp-xy! (sdl2:make-rect -325  615 12 34)
                             (sdl2:make-rect  600 -850 56 78)
                             -1)))

  (test "t > 1 extrapolates beyond the second rect"
        '(1525 -2315 12 34)
        (sdl2:rect->list
         (sdl2:rect-lerp-xy! (sdl2:make-rect -325  615 12 34)
                             (sdl2:make-rect  600 -850 56 78)
                             2)))

  (test-assert "Modifies and returns the first rect by default"
    (let ((r1 (sdl2:make-rect -325  615 12 34))
          (r2 (sdl2:make-rect  600 -850 56 78)))
      (eq? r1 (sdl2:rect-lerp-xy! r1 r2 0.25))))

  (test "Modifies and returns the given dest rect"
        (list #t '(-93 248 12 34))
        (let ((r1 (sdl2:make-rect -325  615 12 34))
              (r2 (sdl2:make-rect  600 -850 56 78))
              (dest (sdl2:make-rect 123 456)))
          (list (eq? dest (sdl2:rect-lerp-xy! r1 r2 0.25 dest))
                (sdl2:rect->list dest)))))


(test-group "sdl2:rect-lerp-xy"
  (test "Returns a new rect without modifying the originals"
        '((-325  615 12 34)
          ( 600 -850 56 78)
          ( -93  248 12 34))
        (let* ((r1 (sdl2:make-rect -325  615 12 34))
               (r2 (sdl2:make-rect  600 -850 56 78))
               (result (sdl2:rect-lerp-xy r1 r2 0.25)))
          (list (sdl2:rect->list r1)
                (sdl2:rect->list r2)
                (sdl2:rect->list result)))))



(versioned-test-group "sdl2:point-in-rect?"
    libSDL-2.0.4+
  (test "returns #t if point is inside rect"
        #t
        (sdl2:point-in-rect?
         (sdl2:make-point 2 3)
         (sdl2:make-rect  1 2 3 4)))

  (test "returns #f if point is outside rect"
        #f
        (sdl2:point-in-rect?
         (sdl2:make-point 0 1)
         (sdl2:make-rect  1 2 3 4))))


(test-group "sdl2:enclose-points"
  (test-assert "Accepts a list of points"
    (sdl2:rect=?
     (sdl2:make-rect -5 -8 16 23)
     (sdl2:enclose-points
      (list (sdl2:make-point -5  2)
            (sdl2:make-point 10 -8)
            (sdl2:make-point 3  14)))))

  (test-assert "Accepts a vector of points"
    (sdl2:rect=?
     (sdl2:make-rect -5 -8 16 23)
     (sdl2:enclose-points
      (vector (sdl2:make-point -5  2)
              (sdl2:make-point 10 -8)
              (sdl2:make-point 3  14)))))

  ;; TODO: Test second return value, clip rect, and rect-out.
  )

;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2019  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


;;; Call a function binding, then test the result (i.e. return value)
;;; to see if the function call succeeded or failed. If it failed,
;;; raise an sdl2 exception. Otherwise, return the original result.
;;;
;;; Usage:
;;;
;;;   (try-call (SDL_Foo arg ...)
;;;             fail?:   fail-predicate
;;;             on-fail: cleanup-expression)
;;;
;;; fail?: and on-fail: are optional keyword arguments. Either or both
;;; may be omitted, and they can be given in either order.
;;;
;;; - fail?: A procedure that is called with the function result.
;;;   The procedure must return a true value if the result indicates
;;;   failure, or #f if it indicates success. Defaults to `negative?'.
;;;
;;; - on-fail: An expression that is executed if the function failed,
;;;   before the exception is raised. This should be used to perform
;;;   any necessary clean up. Defaults to `(begin)'.
;;;
;;; Thanks to megane for creating try-call.
;;;
(define-syntax try-call
  (syntax-rules (fail?: on-fail:)
    ((try-call (func arg ...) fail?: fail? on-fail: on-fail)
     (let ((result (func arg ...)))
       (if (fail? result)
           (begin on-fail
                  (abort (sdl-failure (symbol->string 'func)
                                      result)))
           result)))
    ;; Flip the optional argument order.
    ((try-call call on-fail: on-fail fail?: fail?)
     (try-call call fail?: fail? on-fail: on-fail))
    ;; Default fail?
    ((try-call call on-fail: on-fail)
     (try-call call fail?: negative? on-fail: on-fail))
    ;; Default on-fail
    ((try-call call fail?: fail?)
     (try-call call fail?: fail? on-fail: (begin)))
    ;; Default fail? and on-fail
    ((try-call call)
     (try-call call fail?: negative? on-fail: (begin)))))


(: %struct-fail
   ((* -> boolean) -> (* -> boolean)))
;;; Build a fail? procedure for use with try-call. Given a struct type
;;; predicate, returns a procedure that returns true unless given a
;;; non-null instance of the struct type.
(define (%struct-fail type?)
  (lambda (x)
    (or (not (type? x)) (struct-null? x))))

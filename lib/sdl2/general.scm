;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2019  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export init!
        init-subsystem!
        quit!
        quit-subsystem!
        was-init
        set-main-ready!

        clear-error!
        get-error
        set-error!

        get-platform

        screen-saver-enabled?
        screen-saver-enabled-set!

        has-clipboard-text?
        get-clipboard-text
        set-clipboard-text!

        current-version
        compiled-version
        version-at-least?)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; INITIALIZATION AND SHUTDOWN

(: init!
   (#!optional enum-list -> void))
(define (init! #!optional (flags '(everything)))
  (try-call (SDL_Init (pack-init-flags flags)))
  (void))

(: init-subsystem!
   (enum-list -> void))
(define (init-subsystem! flags)
  (try-call (SDL_InitSubSystem (pack-init-flags flags)))
  (void))

(: quit!
   (-> void))
(define (quit!)
  (SDL_Quit))

(: quit-subsystem!
   (enum-list -> void))
(define (quit-subsystem! flags)
  (SDL_QuitSubSystem (pack-init-flags flags)))

(: was-init
   (#!optional enum-list -> (list-of symbol)))
(define (was-init #!optional (flags '(everything)))
  (unpack-init-flags
   (SDL_WasInit (pack-init-flags flags))
   #t))

(: set-main-ready!
   (-> void))
(define (set-main-ready!)
  (SDL_SetMainReady))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ERROR HANDLING

(: clear-error!
   (-> void))
(define (clear-error!)
  (SDL_ClearError))

(: get-error
   (-> string))
(define (get-error)
  (SDL_GetError))

(: set-error!
   (string -> void))
(define (set-error! message)
  (SDL_SetError message))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PLATFORM

(: get-platform
   (-> string))
(define (get-platform)
  (SDL_GetPlatform))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SCREEN SAVER

(: screen-saver-enabled?
   (-> boolean))
(define (screen-saver-enabled?)
  (SDL_IsScreenSaverEnabled))

(: screen-saver-enabled-set!
   (boolean -> void))
(define (screen-saver-enabled-set! enabled?)
  (case enabled?
    ((#t) (SDL_EnableScreenSaver))
    ((#f) (SDL_DisableScreenSaver))
    (else (error 'screen-saver-enabled-set!
                 "not a boolean" enabled?))))

(set! (setter screen-saver-enabled?)
      screen-saver-enabled-set!)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CLIP BOARD

(: has-clipboard-text?
   (-> boolean))
(define (has-clipboard-text?)
  (SDL_HasClipboardText))

(: get-clipboard-text
   (-> string))
(define (get-clipboard-text)
  (try-call (SDL_GetClipboardText)
            fail?: not))

(: set-clipboard-text!
   (string -> void))
(define (set-clipboard-text! text)
  (try-call (SDL_SetClipboardText text))
  (void))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VERSION

(: current-version
   (-> (list fixnum fixnum fixnum)))
(define (current-version)
  (with-temp-mem ((major-out (%allocate-Uint8))
                  (minor-out (%allocate-Uint8))
                  (patch-out (%allocate-Uint8)))
    (%current-version major-out minor-out patch-out)
    (list (pointer-u8-ref major-out)
          (pointer-u8-ref minor-out)
          (pointer-u8-ref patch-out))))

(: compiled-version
   (-> (list fixnum fixnum fixnum)))
(define (compiled-version)
  (with-temp-mem ((major-out (%allocate-Uint8))
                  (minor-out (%allocate-Uint8))
                  (patch-out (%allocate-Uint8)))
    (%compiled-version major-out minor-out patch-out)
    (list (pointer-u8-ref major-out)
          (pointer-u8-ref minor-out)
          (pointer-u8-ref patch-out))))

(: version-at-least?
   (fixnum fixnum fixnum -> boolean))
(define (version-at-least? x y z)
  (SDL_VERSION_ATLEAST x y z))
